# yoshz/ci-php

PHP Docker image with the following CI tools pre-installed:

* composer
* php-cs-fixer
* local-php-security-checker
* xdebug
