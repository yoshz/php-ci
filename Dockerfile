FROM php:8-cli-alpine

RUN apk add --no-cache git bash make

# enable development config
RUN mv $PHP_INI_DIR/php.ini-development $PHP_INI_DIR/php.ini && \
    sed -i -e 's/^memory_limit =.*/memory_limit = -1/' -e 's/^variables_order =.*/variables_order = "EGPCS"/' $PHP_INI_DIR/php.ini

# install extension (see also: https://github.com/mlocati/docker-php-extension-installer)
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN install-php-extensions pdo_mysql pdo_pgsql pcntl amqp xdebug intl redis xsl

# install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir /usr/local/bin/ --filename=composer && \
    composer --version

# install php-cs-fixer
RUN curl -sS https://cs.symfony.com/download/php-cs-fixer-v3.phar -o /usr/local/bin/php-cs-fixer && \
    chmod +x /usr/local/bin/php-cs-fixer && \
    php-cs-fixer --version

# install local-php-security-checker
RUN curl -sSL https://github.com/fabpot/local-php-security-checker/releases/download/v1.0.0/local-php-security-checker_1.0.0_linux_amd64 -o /usr/local/bin/local-php-security-checker && \
    chmod +x /usr/local/bin/local-php-security-checker && \
    local-php-security-checker -help

# configure default user
RUN mkdir -p /app && chown www-data. /app
WORKDIR /app
USER www-data
CMD [ "/bin/bash" ]
